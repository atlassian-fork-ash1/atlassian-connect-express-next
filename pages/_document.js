import Document, { Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'

export default class ConnectDocument extends Document {
  static getInitialProps({ renderPage }) {
    const { html, head } = renderPage()
    const styles = flush()
    return { html, head, styles }
  }

  getHostScript() {
    const nextData = this.props.__NEXT_DATA__;
    if (nextData && nextData.query && nextData.query.hostScriptUrl) {
      return <script src={nextData.query.hostScriptUrl} type="text/javascript"></script>
    }
  }

  render() {
    return (
      <html>
        <Head>
          {this.getHostScript()}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}